<?php

namespace app\controllers;

use Yii;
use app\models\Alquileres;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Coches;


/**
 * AlquileresController implements the CRUD actions for Alquileres model.
 */
class AlquileresController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Alquileres models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Alquileres::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Alquileres model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Alquileres model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Alquileres();
        
        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigoAlquiler]);
        }
         
        /* devuelve todos los usuarios */
        /*$usuarios= \app\models\Usuarios::find()->all();*/
        /* map coge la información de usuarios y transforma el array en un array con índice codigoUsuario y contenido nombre*/
        /*$usuarios= \yii\helpers\ArrayHelper::map($usuarios,"codigoUsuario","nombre");*/
        
         
    
        /* map coge la información de usuarios y transforma el array en un array con índice codigoUsuario y contenido nombre*/
        /* Cuando se usa una función del tipo concat en una consulta hay que hacerla con el modelo de array, con corchetes*/
        /* Control dependiente de una consulta */
        /*$coches = \app\models\Coches::find()->select(["codigoCoche","CONCAT_WS(' ',codigoCoche,marca) marca"])->all();*/
        /*$coches= \yii\helpers\ArrayHelper::map($coches,"codigoCoche","marca");*/
  
        $usuarios=Alquileres::getUsuarios();
        $coches= Alquileres::getCoches();
         
        return $this->render('create', [
            'model' => $model,
            /*'usuarios'=>$usuarios,
            'coches'=>$coches,*/
        ]);
    }

    /**
     * Updates an existing Alquileres model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigoAlquiler]);
            
        }

        /* devuelve todos los usuarios */
        /*$usuarios= \app\models\Usuarios::find()->all();*/
        /* map coge la información de usuarios y transforma el array en un array con índice codigoUsuario y contenido nombre*/
        /*$usuarios= \yii\helpers\ArrayHelper::map($usuarios,"codigoUsuario","nombre");*/
        
         
    
        /* map coge la información de usuarios y transforma el array en un array con índice codigoUsuario y contenido nombre*/
        /* Cuando se usa una función del tipo concat en una consulta hay que hacerla con el modelo de array, con corchetes*/
        /* Control dependiente de una consulta */
        /*$coches = \app\models\Coches::find()->select(["codigoCoche","CONCAT_WS(' ',codigoCoche,marca) marca"])->all();*/
        /*$coches= \yii\helpers\ArrayHelper::map($coches,"codigoCoche","marca");*/
        
        $usuarios=Alquileres::getUsuarios();
        $coches= Alquileres::getCoches();
        
        return $this->render('update', [
            'model' => $model,
            /*'usuarios'=>$usuarios,
            'coches'=>$coches,*/
        ]);
    }

    /**
     * Deletes an existing Alquileres model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Alquileres model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Alquileres the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Alquileres::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionPrueba(){
        $listado= Alquileres::listar();
        var_dump($listado);
    }
    public function actionPrueba2(){
        $listado=Alquileres::findOne(1);
        var_dump($listado->coche0);
    }
    
    public function actionAlquileresusuarios($id){
          
        $dataProvider = new ActiveDataProvider([
            'query' => Alquileres::getAlquileresusuario($id),
        ]);
        
        return $this->render("alquileresusuarios",[
            'dataProvider'=>$dataProvider,
        ]);
    }
    
    public function actionAlquileresyear($id){
        $dataProvider = new ActiveDataProvider([
            'query' => Alquileres::getAlquileresYear($id),
        ]);
        
        return $this->render("alquileresyear",[
            'dataProvider'=>$dataProvider,
        ]);
    }
    
    public function actionAlquilerescoche($id){
        $dataProvider = new ActiveDataProvider([
            'query' => Alquileres::getAlquileresCoche($id),
        ]);
        
        return $this->render("alquilerescoche",[
            'dataProvider'=>$dataProvider,
        ]);
    }
    
    public function actionAlquileresmarca($id){
        
        /* para usar getAlquileresMarca del modelo de Alquileres */
        $coche=Coches::findOne($id);
       
        
//        $dataProvider = new ActiveDataProvider([
//            'query' => Alquileres::getAlquileresMarca($coche->marca)
//        ]);
        
        $dataProvider=new ActiveDataProvider([
            'query'=>$coche->getAlquileresMarca(),
        ]);
        
        return $this->render("alquileresmarca",[
            'dataProvider'=>$dataProvider,
            
        ]);
    }
}
