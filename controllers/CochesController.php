<?php

namespace app\controllers;

use Yii;
use app\models\Coches;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CochesController implements the CRUD actions for Coches model.
 */
class CochesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Coches models.
     * @return mixed
     */
    public function actionIndex($id=0)
    {
        $coche=Coches::findOne($id);
        
        
//        $dataProvider = new ActiveDataProvider([
//            'query' => Alquileres::getAlquileresMarca($coche->marca)
//        ]);
        
        
        
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => Coches::find(),
        ]);
        
        $dataProvider2=new ActiveDataProvider([
            'query'=>$coche->getAlquileresCoche(),
        ]);
        
        $dataProvider3 = new ActiveDataProvider([
            'query' => $coche->getAlquileresMarca(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,$dataProvider2, dataProvider3,
             
            ]);
    }

    /**
     * Displays a single Coches model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Coches model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Coches();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigoCoche]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Coches model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigoCoche]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Coches model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Coches model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Coches the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Coches::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionCochesalquileres($id){
        
        /* En el id se está pasando el codigo del coche */
        
        /* obtengo el coche correspondiente al código pasado*/
        $coche=Coches::findOne($id);
        
        
//        $dataProvider = new ActiveDataProvider([
//            'query' => Alquileres::getAlquileresMarca($coche->marca)
//        ]);
        
        $dataProvider=new ActiveDataProvider([
            'query'=>$coche->getAlquileresCoche(),
        ]);
        
        return $this->render("cochesalquileres",[
            'dataProvider'=>$dataProvider,
            
        ]);
    }
    
    public function actionMarcaalquileres($id){
        
        $coche=Coches::findOne($id);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $coche->getAlquileresMarca(),
        ]);
                
        return $this->render("marcasalquileres",[
           'dataProvider'=>$dataProvider, 
        ]);
        
    }
}
