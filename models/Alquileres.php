<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alquileres".
 *
 * @property int $codigoAlquiler
 * @property int $usuario
 * @property int $coche
 * @property string $fecha
 *
 * @property Coches $coche0
 * @property Usuarios $usuario0
 */
class Alquileres extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    /* Atributo donde se almacena el año de la función getAlquileresYear */
    public $amo;
    
    public static function tableName()
    {
        return 'alquileres';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario', 'coche'], 'integer'],
            [['fecha'], 'safe'],
            [['usuario', 'coche', 'fecha'], 'unique', 'targetAttribute' => ['usuario', 'coche', 'fecha']],
            [['coche'], 'exist', 'skipOnError' => true, 'targetClass' => Coches::className(), 'targetAttribute' => ['coche' => 'codigoCoche']],
            [['usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['usuario' => 'codigoUsuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoAlquiler' => 'Codigo Alquiler',
            'usuario' => 'Usuario',
            'coche' => 'Coche',
            'fecha' => 'Fecha de alquiler',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoche0()
    {
        return $this->hasOne(Coches::className(), ['codigoCoche' => 'coche']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario0()
    {
        return $this->hasOne(Usuarios::className(), ['codigoUsuario' => 'usuario']);
    }
    
    /*public function getFecha0(){
        return $this->hasOne(Usuarios::className(), ['codigoUsuario' => 'usuario']);
    }*/
   
    public function afterFind() {
        parent::afterFind();
        // los listados de fecha me salen en castellano
        /* con este código añadido en el after find evitaríamos tener que sacar el año en la consulta de la función getAlquileresYear */
        /* $this->amo = Yii::$app->formatter->asDate($this->fecha, 'Y');*/
        $this->fecha = Yii::$app->formatter->asDate($this->fecha, 'php:d/m/Y');
        
    }
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        //$this->fecha = Yii::$app->formatter->asDate($this->fecha, 'php:Y/m/d'); -- no funciona porque la fecha no está guardada en el formato válido de MySql
        // Cuando se usa \DateTime es para que busque datetime de php, no de models
        // Con la función createFromFormat cambiamos el formato de la fecha del formato en castellano al formato que entiende SQL
        $this->fecha= \DateTime::createFromFormat("d/m/Y", $this->fecha)->format("Y/m/d");
        return true;
    }
    
    public static function listar(){
        /* Se usa self porque es un método estático */
        return self::find()->all();
    }
    
    public function getUsuarios(){
        /* Devuelve un array con índice codigoUsuario y contenido el nombre de los usuarios */
        $usuarios=Usuarios::find()->all();
        return $usuarios= \yii\helpers\ArrayHelper::map($usuarios,"codigoUsuario","nombre");
    }
    
    public function getCoches(){
        /* Devuelve un array con los código de los coches como índice y una concatenación del código del coche y la marca como contenido */
        $coches=Coches::find()->select(["codigoCoche","CONCAT_WS(' ',codigoCoche,marca) marca"])->all();
        return $coches= \yii\helpers\ArrayHelper::map($coches,"codigoCoche","marca");
        
    }
    
    public static function getAlquileresusuario($id){
          
        return self::find()
                ->where("usuario=$id");
    }
    
    public static function getAlquileresYear($id){
        
        /* SELECT 
         *  YEAR(fecha) 
         * FROM alquileres 
         * WHERE codigoAlquiler=$id */
        //esto devuelve un registro que es un modelo
        $registro=self::find()
                ->select("YEAR(fecha) amo")
                ->where("codigoAlquiler=$id")
                /* ->asArray() convertiría el modelo en un array y no haría falta la propiedad amo */
                ->one();
        
        /* Si tuviéramos un array el acceso sería $anno=$registro["amo"] */
        $anno=$registro->amo;
        
        /* SELECT * FROM ALQUILERES WHERE YEAR(FECHA)=$ANNO */
        return self::find()
                ->select('*,YEAR(fecha) amo')
                ->where("YEAR(fecha)=$anno");
    }
    
    public static function getAlquileresCoche($id){
        return self::find()
                ->where("coche=$id");
        
    }
    
    public static function getAlquileresMarca($marca){
        $m=self::find()->joinWith('coche0',true,'inner join')->andWhere("marca='$marca'");
        
        
        return $m;
    }
    
}
