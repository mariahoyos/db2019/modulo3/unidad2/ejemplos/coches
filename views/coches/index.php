<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Coches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coches-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Coches', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            /*'codigoCoche',*/
            [
                /* Si el nombre del atributo existe coge el nombr en azul y se puede ordenar por él */
                'attribute'=>'codigoCoche',
                'label'=>'Código del coche',
                'format'=>'raw',/* Cuando quieres escribir código y que el navegador lo interprete */
                'content'=>function($model){
                            return Html::a(
                                    /* En este enlace se dice a qué controlador-acción se llama y se le pasa el parámetro id */
                                    'ver alquileres del coche: '.$model->codigoCoche,
                                    [
                                        'coches/cochesalquileres',
                                        /* El ID que le pasamos es el id coche que es un atributo del modelo */
                                        'id'=>$model->codigoCoche
                                    ],
                                    [
                                        'class'=>'btn btn-info'
                                    ]
                            );
                }
                
            ],        
            /*'marca',*/
            [
                /* Si el nombre del atributo existe coge el nombr en azul y se puede ordenar por él */
                'attribute'=>'marca',
                'label'=>'Marca del coche',
                'format'=>'raw',/* Cuando quieres escribir código y que el navegador lo interprete */
                'content'=>function($model){
                            return Html::a(
                                    /* En este enlace se dice a qué controlador-acción se llama y se le pasa el parámetro id */
                                    'ver alquileres de la marca: '.$model->marca,
                                    [
                                        'coches/marcaalquileres',
                                        /* El ID que le pasamos es el id coche que es un atributo del modelo */
                                        'id'=>$model->codigoCoche
                                    ],
                                    [
                                        'class'=>'btn btn-info'
                                    ]
                            );
                }
                
            ],        
                    
            'color',

            ['class' => 'yii\grid\ActionColumn',
                        'template'=>'{view}{update}{delete}{coches}',    
                        'buttons'=>[
                        'coches'=>function($url,$model){
                        return Html::a('<span class = "glyphicon glyphicon-cloud-download"></span>',['coches/index','id'=>$model->codigoCoche]);
                    
                        },
            
                
                
                
                        ],
                
                
            ],
        ],
    ]); ?>


</div>
