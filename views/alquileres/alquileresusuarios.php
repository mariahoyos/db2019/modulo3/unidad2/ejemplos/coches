<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alquileres-index">

    <div class="jumbotron">
  <h2>Estos son los alquileres de <?=$dataProvider->models[0]->usuario0->nombre ?></h2>
  
  
  <p>
   <?= Html::a(
          "Volver a alquileres",
          ['alquileres/index'],
          [
              'class'=>'btn btn-primary btn-ms'
          ]
          );  ?>
  </p>
    </div>

    
    


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'codigoAlquiler',
            'coche',       
            'fecha',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

