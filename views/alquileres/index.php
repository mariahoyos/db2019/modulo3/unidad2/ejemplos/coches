<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alquileres';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alquileres-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Alquileres', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'codigoAlquiler',
            'usuario',
            /* Con las funciones getter se puede usar la notación de punto para devolver un campo del registro que devuelve */
            /* SÓLO EN EL GRIDVIEW */
            /*'usuario0.nombre',*/
            [
                'label'=>'Nombre del usuario',
                'format'=>'raw',/* Cuando quieres escribir código y que el navegador lo interprete */
                'content'=>function($model){
                            return Html::a(
                                    /* En este enlace se dice a qué controlador-acción se llama y se le pasa el parámetro id */
                                    'ver alquileres de '.$model->usuario0->nombre,
                                    [
                                        'alquileres/alquileresusuarios',
                                        /* El ID que le pasamos es coche que es un atributo del modelo */
                                        'id'=>$model->coche
                                    ],
                                    [
                                        'class'=>'btn btn-info'
                                    ]
                            );
                }
                
            ],
            /*'coche',*/
            [
                /* Si el nombre del atributo existe coge el nombr en azul y se puede ordenar por él */
                'attribute'=>'coche',
                'label'=>'Código del coche',
                'format'=>'raw',/* Cuando quieres escribir código y que el navegador lo interprete */
                'content'=>function($model){
                            return Html::a(
                                    /* En este enlace se dice a qué controlador-acción se llama y se le pasa el parámetro id */
                                    'ver alquileres del coche: '.$model->coche,
                                    [
                                        'alquileres/alquilerescoche',
                                        /* El ID que le pasamos es el id coche que es un atributo del modelo */
                                        'id'=>$model->coche
                                    ],
                                    [
                                        'class'=>'btn btn-info'
                                    ]
                            );
                }
                
            ],        
            /*'coche0.marca',*/
            [
                /* Si el nombre del atributo existe coge el nombr en azul y se puede ordenar por él */
                'label'=>'Marca del coche',
                'format'=>'raw',/* Cuando quieres escribir código y que el navegador lo interprete */
                'content'=>function($model){
                            return Html::a(
                                    /* En este enlace se dice a qué controlador-acción se llama y se le pasa el parámetro id */
                                    'ver alquileres de la marca: '.$model->coche0->marca,
                                    [
                                        'alquileres/alquileresmarca',
                                        /* El ID que le pasamos es el id coche que es un atributo del modelo */
                                        'id'=>$model->coche
                                    ],
                                    [
                                        'class'=>'btn btn-info'
                                    ]
                            );
                }
                
            ],           
            /*'fecha',*/
            [
                'label'=>'Fecha del alquiler',
                'format'=>'raw',/* Cuando quieres escribir código y que el navegador lo interprete */
                'content'=>function($model){
                            return Html::a(
                                    /* En este enlace se dice a qué controlador-acción se llama y se le pasa el parámetro id */
                                    'ver alquileres del año de la fecha: '.$model->fecha,
                                    [
                                        'alquileres/alquileresyear',
                                        /* El ID que le pasamos es el código de alquiler que es un atributo del modelo */
                                        /* No se le envía la fecha porque poner la fecha puede dar problemas al ponerlo en la url */
                                        'id'=>$model->codigoAlquiler
                                    ],
                                    [
                                        'class'=>'btn btn-info'
                                    ]
                            );
                }
                
            ],

            ['class' => 'yii\grid\ActionColumn'],
         ]
    ]); ?>


</div>

