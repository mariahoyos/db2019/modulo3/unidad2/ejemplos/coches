<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alquileres';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="alquileres-index">
    
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="jumbotron">
  
  <h2>Estos son los alquileres del coche: <?=$dataProvider->models[0]->coche0->marca ?></h2>
  
  <p>
   <?= Html::a(
          "Volver a alquileres",
          ['alquileres/index'],
          [
              'class'=>'btn btn-primary btn-ms'
          ]
          );  ?>
  </p>
    </div>

    
    


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'codigoAlquiler',
            'coche',
            'coche0.marca',
            'usuario',    
            'fecha',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>