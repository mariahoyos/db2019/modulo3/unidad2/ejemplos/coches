<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\Alquileres */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alquileres-form">

    <?php $form = ActiveForm::begin(); ?>
    
    
    
     <?=$form->field($model, 'usuario')->dropDownList($model->usuarios) ?>    
   
    
    <?=$form->field($model, 'coche')->dropDownList($model->coches) ?>

 
    
    <!-- En el caso de no tener la posibilidad de poner un label en el widget se haría esto
    (leer el nombre del atributo del modelo) [utilizando activeRecord para el label]-->
    <?php //echo'<label class="control-label">'.$model->getAttributeLabel("fecha").'</label>'?>
    
    <?= $form->field($model, 'fecha')->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Seleccione la fecha del alquiler'],
    'pluginOptions' => [
        'autoclose'=>true,
        'todayHighlight' => true,
        'todayBtn' => true,
        //'format'=>'dd/mm/yyyy'
    ]
    ]);?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
